import {EnvProperties} from '../app/openaireLibrary/utils/properties/env-properties';

export let properties: EnvProperties = {
  environment: 'beta',
  useCache: true,
  useLongCache: true,
  domain : "https://usagecounts.openaire.eu",
  baseLink: "",
  enablePiwikTrack: true,
  piwikBaseUrl: "https://analytics.openaire.eu/piwik.php?idsite=",
  piwikSiteId: "",
  metricsAPIURL: 'https://beta.services.openaire.eu/usagestats/',
  loginUrl: "https://beta.services.openaire.eu/admin-user-management/openid_connect_login",
  userInfoUrl: "https://beta.services.openaire.eu/uoa-user-management/api/users/getUserInfo?accessToken=",
  logoutUrl: "https://aai.openaire.eu/proxy/saml2/idp/SingleLogoutService.php?ReturnTo=",
  cookieDomain: ".openaire.eu",
  cacheUrl: "https://demo.openaire.eu/cache/get?url=",
  adminToolsAPIURL: "https://beta.services.openaire.eu/uoa-admin-tools/",
  reCaptchaSiteKey: "6LezhVIUAAAAAOb4nHDd87sckLhMXFDcHuKyS76P",
  admins: ["feedback@openaire.eu"],
  sushiliteURL: 'https://services.openaire.eu/usagestats/sushilite/',
  footerGrantText: "OpenAIRE has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541 and 101017452"
};
