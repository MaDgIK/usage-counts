// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import {EnvProperties} from '../app/openaireLibrary/utils/properties/env-properties';

export let properties: EnvProperties = {
  environment: 'development',
  useCache: false,
  useLongCache: true,
  domain : "https://usagecounts.openaire.eu",
  baseLink: "",
  enablePiwikTrack: false,
  piwikBaseUrl: "https://analytics.openaire.eu/piwik.php?idsite=",
  piwikSiteId: "",
  metricsAPIURL: 'https://services.openaire.eu/usagestats/',
  loginUrl: 'http://dl170.madgik.di.uoa.gr:8180/dnet-login/openid_connect_login',
  userInfoUrl: 'http://dl170.madgik.di.uoa.gr:8180/dnet-openaire-users-1.0.0-SNAPSHOT/api/users/getUserInfo?accessToken=',
  logoutUrl: 'https://aai.openaire.eu/proxy/saml2/idp/SingleLogoutService.php?ReturnTo=',
  cookieDomain: '.di.uoa.gr',
  cacheUrl: "https://demo.openaire.eu/cache/get?url=",
  adminToolsAPIURL: 'http://duffy.di.uoa.gr:8080/uoa-admin-tools/',
  reCaptchaSiteKey: '6LcVtFIUAAAAAB2ac6xYivHxYXKoUvYRPi-6_rLu',
  admins: ['kostis30fylloy@gmail.com'],
  sushiliteURL: 'http://scoobydoo.di.uoa.gr/static/sushilite/',
  footerGrantText: "OpenAIRE has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreements No. 777541 and 101017452"
};
