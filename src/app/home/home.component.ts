import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {FullPageSliderComponent} from '../openaireLibrary/utils/full-page-slider/full-page-slider.component';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {Subscriber, Subscription} from 'rxjs';
import {properties} from '../../environments/environment';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  
  public initSlide: number = 1;
  public fragments: string[] = ['', 'why', 'who', 'features'];
  @ViewChild('slider', { static: true }) slider: FullPageSliderComponent;
  properties: EnvProperties = properties;
  
  subs: Subscription[] = [];
  
  constructor(private route: ActivatedRoute,
              private router: Router,
              private _title: Title, private _piwikService: PiwikService,
              private _meta: Meta, private seoService: SEOService
  ) {
  }
  
  ngOnInit() {
    let description = 'UsageCounts service collects usage data from Open Science content providers repositories, journals, and other scientific data sources. Then, it aggregates them, delivering standardized activity reports about research usage and uptake. It complements existing citation mechanisms and assists institutional repository managers, research communities, research organizations, funders, and policy makers to track and evaluate research from an early stage.';
    let title = 'OpenAIRE - UsageCounts';
    this._title.setTitle(title);
    this._meta.updateTag({content: description}, 'name=\'description\'');
    this._meta.updateTag({content: description}, 'property=\'og:description\'');
    this._meta.updateTag({content: title}, 'property=\'og:title\'');
    this._title.setTitle(title);
    let url = this.properties.domain + this.properties.baseLink + this.router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this._meta.updateTag({content: url}, 'property=\'og:url\'');
    if (this.properties.enablePiwikTrack && (typeof document !== 'undefined')) {
      this.subs.push(this._piwikService.trackView(this.properties, title).subscribe());
    }
    this.subs.push(this.route.fragment.subscribe(name => {
      if (name) {
        let slide = this.fragments.findIndex(fragment => fragment === name) + 1;
        if (slide > 0) {
          this.initSlide = slide;
        }
      }
    }));
    this.subs.push(this.slider.stateAsObservable.subscribe(state => {
      if (state == 1) {
        this.router.navigate(['/']);
      } else {
        this.router.navigate(['/'], {fragment: this.fragments[state - 1]});
      }
    }));
  }
  
  public ngOnDestroy() {
    this.subs.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  
}
