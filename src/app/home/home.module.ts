import {NgModule} from '@angular/core';
import {HomeComponent} from './home.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FullPageSliderModule} from '../openaireLibrary/utils/full-page-slider/full-page-slider.module';
import {BottomModule} from '../openaireLibrary/sharedComponents/bottom.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '', component: HomeComponent,
    }]),
    FullPageSliderModule,
    BottomModule,
  ],
  declarations: [HomeComponent],
  exports: [HomeComponent]
})
export class HomeModule{
}
