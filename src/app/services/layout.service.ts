import {Injectable} from '@angular/core';
import {ActivationStart, Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  
  private isHomeSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  
  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof ActivationStart) {
        let data = event.snapshot.data;
        if (data['isHome'] == undefined ||
          data['isHome'] === false) {
          this.setIsHome(false);
        } else {
          this.setIsHome(true);
        }
      }
    });
  }
  
  public setIsHome(isHome: boolean) {
    this.isHomeSubject.next(isHome);
  }
  
  public get isHome(): Observable<boolean> {
    return this.isHomeSubject.asObservable();
  }
}
