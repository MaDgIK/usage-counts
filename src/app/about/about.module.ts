import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";

import {AboutComponent} from "./about.component";
import {Schema2jsonldModule} from '../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module';
import {IconsModule} from '../openaireLibrary/utils/icons/icons.module';
import {IconsService} from '../openaireLibrary/utils/icons/icons.service';
import {arrow_down} from '../openaireLibrary/utils/icons/icons';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '', component: AboutComponent
    }]),
    Schema2jsonldModule,
    IconsModule
  ],
  declarations: [AboutComponent],
  exports: [AboutComponent]
})
export class AboutModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([arrow_down]);
  }
}
