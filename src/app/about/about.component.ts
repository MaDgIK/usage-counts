import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {faqs} from './faqs';
import {ActivatedRoute, Router} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {properties} from '../../environments/environment';
import {Subscriber, Subscription} from 'rxjs';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';

@Component({
  selector: 'about',
  templateUrl: 'about.component.html',
  styleUrls: ['about.component.css'],
})
export class AboutComponent implements OnInit, OnDestroy {
  faqs: any[] = faqs;
  properties: EnvProperties = properties;
  description = "UsageCounts service is an OpenAIRE service built to cover the needs of content providers and consumers and offer added value to assist them in reaching their goals. UsageCounts forms metrics of usage activity of Open Access Repositories categorizing the data retrieved by country, number of downloads, number of views, number of repositories and all derivative quantitative open metrics, comprehensively. Architecture. ";
  title = "OpenAIRE - UsageCounts | About";
  subs: Subscription[] = [];
  large: boolean = false;
  
  constructor(private route: ActivatedRoute,
              private router: Router,
              private _title: Title, private _piwikService: PiwikService,
              private _meta: Meta, private seoService: SEOService) {
  }
  
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if(event.target.innerWidth > 959 && this.large === false) {
      this.large = true;
    } else if(event.target.innerWidth < 960 && this.large === true){
      this.large = false;
    }
  }
  
  ngOnInit() {
    if(typeof window !== 'undefined') {
      this.large = window.innerWidth > 959;
    }
    this._title.setTitle(this.title);
    this._meta.updateTag({content: this.description}, "name='description'");
    this._meta.updateTag({content: this.description}, "property='og:description'");
    this._meta.updateTag({content: this.title}, "property='og:title'");
    this._title.setTitle(this.title);
    let url = this.properties.domain + this.properties.baseLink + this.router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this._meta.updateTag({content: url}, "property='og:url'");
    if (this.properties.enablePiwikTrack && (typeof document !== 'undefined')) {
      this.subs.push(this._piwikService.trackView(this.properties, this.title).subscribe());
    }
  }
  
  public ngOnDestroy() {
    this.subs.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
}
