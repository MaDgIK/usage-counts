import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";

import {ResourcesComponent} from "./resources.component";
import {Schema2jsonldModule} from '../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module';
import {IconsService} from '../openaireLibrary/utils/icons/icons.service';
import {arrow_down, arrow_right} from '../openaireLibrary/utils/icons/icons';
import {IconsModule} from '../openaireLibrary/utils/icons/icons.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '', component: ResourcesComponent
    }]),
    Schema2jsonldModule,
    IconsModule
  ],
  declarations: [ResourcesComponent],
  exports: [ResourcesComponent]
})
export class ResourcesModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([arrow_right, arrow_down]);
  }
}
