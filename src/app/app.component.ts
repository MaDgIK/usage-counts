import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuItem, RootMenuItem} from './openaireLibrary/sharedComponents/menu';
import {EnvProperties} from './openaireLibrary/utils/properties/env-properties';
import {User} from './openaireLibrary/login/utils/helper.class';
import {properties} from '../environments/environment';
import {LayoutService} from './services/layout.service';
import {Header} from './openaireLibrary/sharedComponents/navigationBar.component';
import {SmoothScroll} from './openaireLibrary/utils/smooth-scroll';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
})

export class AppComponent implements OnInit, OnDestroy {
  title = 'Metadata Registry Service';
  userMenuItems: MenuItem[] = [];
  menuItems: RootMenuItem [] = [];
  logInUrl = null;
  logOutUrl = null;
  properties: EnvProperties = properties;
  showMenu: boolean = false;
  user: User;
  isHome: boolean;
  header: Header;
  logoPath: string = 'assets/common-assets/';
  
  constructor(private layoutService: LayoutService,
              private smoothScroll: SmoothScroll) {}
  
  ngOnInit() {
    this.logInUrl = this.properties.loginUrl;
    this.logOutUrl = this.properties.logoutUrl;
    this.showMenu = true;
    this.layoutService.isHome.subscribe(isHome => {
      this.isHome = isHome
      this.header = {
        route: "/",
        url: null,
        title: 'usage-counts',
        logoUrl: this.logoPath + 'logo-large-usage-counts.png',
        logoSmallUrl:this.logoPath + 'logo-small-usage-counts.png',
        position:'left',
        badge:true
      };
      this.buildMenu(isHome);
    });
  }
  
  buildMenu(isHome) {
    this.menuItems = [
      {
        rootItem: new MenuItem("resources", "Resources", "", "/resources", false, [], null, {}),
        items: [
          new MenuItem("provide", "OpenAIRE Provide", "", "/resources", false, [], null, {}),
          new MenuItem("apis", "APIs and Reports", "", "/resources", false, [], null, {}, null, 'apis')
        ]
      },
      {
        rootItem: new MenuItem("analytics", "Analytics", "", "/analytics", false, [], null, {}),
        items: []
      },
      {
        rootItem: new MenuItem("contact", "Contact", "", "/contact", false, [], null, {}),
        items: []
      },
      {
        rootItem: new MenuItem("about", "About", "", "/about", false, [], null, {}),
        items: [
          new MenuItem("architecture", "Architecture", "", "/about", false, [], null, {}, null, 'architecture'),
/*
          new MenuItem("faqs", "FAQs", "", "/about", false, [], null, {}, null, 'faqs')
*/
        ]
      }
    ];
    if(!isHome) {
      this.menuItems = [{
        rootItem: new MenuItem("home", "Home", "", "/", false, [], null, {}),
        items: []
      }].concat(this.menuItems);
    }
  }
  
  ngOnDestroy() {
    this.smoothScroll.clearSubscriptions();
  }
}
