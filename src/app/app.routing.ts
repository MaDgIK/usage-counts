import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PreviousRouteRecorder} from './openaireLibrary/utils/piwik/previousRouteRecorder.guard';
import {ErrorPageComponent} from './openaireLibrary/error/errorPage.component';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    data: {
      isHome: true
    },
    canDeactivate: [PreviousRouteRecorder]
  },
  {
    path: 'resources',
    loadChildren: () => import('./resources/resources.module').then(m => m.ResourcesModule),
    canDeactivate: [PreviousRouteRecorder]
  },
  {
    path: 'analytics',
    loadChildren: () => import('./analytics/analytics.module').then(m => m.AnalyticsModule),
    canDeactivate: [PreviousRouteRecorder]
  },
  {
    path: 'contact',
    loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule),
    canDeactivate: [PreviousRouteRecorder]
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then(m => m.AboutModule),
    canDeactivate: [PreviousRouteRecorder]
  },
  {
    path: 'sushilite/:id',
    loadChildren: () => import('./sushilite/sushilite.module').then(m => m.SushiliteModule),
    canDeactivate: [PreviousRouteRecorder]
  },
  { path: '**',pathMatch: 'full',component: ErrorPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {
    onSameUrlNavigation: "reload"
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
