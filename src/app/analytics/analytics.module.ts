import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {AnalyticsComponent} from './analytics.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {Schema2jsonldModule} from '../openaireLibrary/sharedComponents/schema2jsonld/schema2jsonld.module';
import {IconsModule} from '../openaireLibrary/utils/icons/icons.module';
import {IconsService} from '../openaireLibrary/utils/icons/icons.service';
import {arrow_down} from '../openaireLibrary/utils/icons/icons';
import {LoadingModule} from '../openaireLibrary/utils/loading/loading.module';
import {SearchInputModule} from '../openaireLibrary/sharedComponents/search-input/search-input.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([{
      path: '', component: AnalyticsComponent
    }]),
    Schema2jsonldModule,
    IconsModule,
    LoadingModule,
    SearchInputModule
  ],
  declarations: [AnalyticsComponent],
  exports: [AnalyticsComponent]
})
export class AnalyticsModule {
  constructor(private iconsService: IconsService) {
    this.iconsService.registerIcons([arrow_down]);
  }
}
