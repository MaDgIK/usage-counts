export interface UsageStat {
  repositories: any;
  total_downloads: any;
  total_views: any;
}

export interface CountryUsageStat {
  country: string;
  views: any;
  downloads:any;
  total_repos: any;
}
