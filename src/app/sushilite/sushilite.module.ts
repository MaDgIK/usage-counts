import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";

import {SushiliteComponent} from "./sushilite.component";
import {IFrameModule} from '../openaireLibrary/utils/iframe.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '', component: SushiliteComponent
    }]),
    IFrameModule
  ],
  declarations: [SushiliteComponent],
  exports: [SushiliteComponent]
})
export class SushiliteModule {

}
