import {Component, HostListener, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {properties} from '../../environments/environment';

@Component({
  selector: 'sushilite',
  templateUrl: 'sushilite.component.html',
  styleUrls: ['sushilite.component.css'],
})
export class SushiliteComponent implements OnInit {
  
  public url: string;
  public reportTitle: string;
  public screenHeight: number;
  
  constructor(private title: Title,
              private route: ActivatedRoute) {
    this.getScreenSize();
  }
  
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.reportTitle = params['id'] + ' Report';
      this.title.setTitle('OpenAIRE - UsageCounts | ' + this.reportTitle);
      this.url = properties.sushiliteURL + params['id'];
    });
  }
  
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight - 100;
  }
}
