import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {EmailService} from '../openaireLibrary/utils/email/email.service';
import {Email} from '../openaireLibrary/utils/email/email';
import {EnvProperties} from '../openaireLibrary/utils/properties/env-properties';
import {Composer} from '../openaireLibrary/utils/email/composer';
import {Meta, Title} from '@angular/platform-browser';
import {HelperFunctions} from '../openaireLibrary/utils/HelperFunctions.class';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {properties} from '../../environments/environment';
import {Subscriber, Subscription} from 'rxjs';
import {PiwikService} from '../openaireLibrary/utils/piwik/piwik.service';
import {SEOService} from '../openaireLibrary/sharedComponents/SEO/SEO.service';

@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['contact.component.css']
})

export class ContactComponent implements OnInit, OnDestroy {
  public showLoading = true;
  public errorMessage = '';
  public email: Email;
  public properties: EnvProperties = properties;
  
  public contactForm: UntypedFormGroup;
  @ViewChild('AlertModal') modal;
  description = 'Any questions? Contact us ';
  title = 'OpenAIRE - UsageCounts |  Contact Us';
  subs: Subscription[] = [];
  
  constructor(private router: Router,
              private emailService: EmailService,
              private _title: Title, private _piwikService: PiwikService,
              private _meta: Meta, private seoService: SEOService,
              private fb: UntypedFormBuilder) {
  }
  
  ngOnInit() {
    this._title.setTitle(this.title);
    this._meta.updateTag({content: this.description}, 'name=\'description\'');
    this._meta.updateTag({content: this.description}, 'property=\'og:description\'');
    this._meta.updateTag({content: this.title}, 'property=\'og:title\'');
    this._title.setTitle(this.title);
    let url = this.properties.domain + this.properties.baseLink + this.router.url;
    this.seoService.createLinkForCanonicalURL(url, false);
    this._meta.updateTag({content: url}, 'property=\'og:url\'');
    if (this.properties.enablePiwikTrack && (typeof document !== 'undefined')) {
      this.subs.push(this._piwikService.trackView(this.properties, this.title).subscribe());
    }
    this.email = {body: '', subject: '', recipients: []};
    this.reset();
    this.showLoading = false;
  }
  
  public ngOnDestroy() {
    this.subs.forEach(subscription => {
      if (subscription instanceof Subscriber) {
        subscription.unsubscribe();
      }
    });
  }
  
  public send(event) {
    HelperFunctions.scroll();
    if (event.valid === true) {
      this.sendMail(this.properties.admins);
    } else {
      this.errorMessage = 'Please fill in all the required fields!';
    }
  }
  
  public reset() {
    this.contactForm = this.fb.group({
      name: this.fb.control('', Validators.required),
      email: this.fb.control('', [Validators.required, Validators.email]),
      affiliation: this.fb.control('', Validators.required),
      organization: this.fb.control('', Validators.required),
      description: this.fb.control('', Validators.required),
      recaptcha: this.fb.control('', Validators.required),
    });
    this.errorMessage = '';
  }
  
  private sendMail(admins: any) {
    this.showLoading = true;
    this.subs.push(this.emailService.contact(this.properties,
      Composer.composeEmailForUsageCounts(this.contactForm.value, admins),
      this.contactForm.value.recaptcha).subscribe(
      res => {
        this.showLoading = false;
        if (res) {
          this.reset();
          this.modalOpen();
        } else {
          this.errorMessage = 'Email sent failed! Please try again.';
          this.contactForm.get('recaptcha').setValue('');
        }
      },
      error => {
        this.handleError('Email sent failed! Please try again.', error);
        this.showLoading = false;
        this.contactForm.get('recaptcha').setValue('');
      }
    ));
  }
  
  public modalOpen() {
    this.modal.okButton = true;
    this.modal.alertTitle = 'Your request has been successfully submitted';
    this.modal.alertMessage = false;
    this.modal.cancelButton = false;
    this.modal.okButtonLeft = false;
    this.modal.okButtonText = 'OK';
    this.modal.open();
  }
  
  handleError(message: string, error) {
    this.errorMessage = message;
    console.log('Server responded: ' + error);
    this.showLoading = false;
  }
  
  public goToHome() {
    this.router.navigate(['/']);
  }
}
